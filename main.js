// 1. Це процес, за допомогою якого змінюються поведінки певних символів або послідовностей символів, щоб вони 
//    були використані не в своєму звичному значенні, а в іншому контексті. Тобто допамагає уникнути непередбачувані
//    результати при використанны спеціальних символів, наприклад "\"
// 2. З використанням "function", наприклад 'function myFunction(..., ...) { }'
//    Функціональний вираз, приклад 'const myFunction = function(..., ...) { }'
//    Функція стрілкова, приклад 'const myFunction = (param1, param2) => { }'
// 3. Це коли створенні змінні та функції переміщаються вище свого місця в коді, до самого початку виконання

function createNewUser() {
  let firstName = prompt("Введіть ім'я:");
  let lastName = prompt("Введіть прізвище:");
  let birthday = prompt("Введіть дату народження (dd.mm.yyyy):");

  while (!validDate(birthday)) {
    birthday = prompt("Помилка! Введіть дату народження в правильному форматі (dd.mm.yyyy):");
  }
  this.birthday = birthday;
  
  // Перевірка дати на вище вказаний формат
  function validDate(dateString) {
    let format = /^\d{2}\.\d{2}\.\d{4}$/;
    return format.test(dateString);
  }
    
  const newUser = {
    getLogin: function() {
      return (firstName.charAt(0) + lastName).toLowerCase();
    },

    getAge: function() {
      const NOW = new Date();
      const BIRTHDATE = new Date(birthday.split('.').reverse());
      let age = NOW.getFullYear() - BIRTHDATE.getFullYear();
      let month = NOW.getMonth() - BIRTHDATE.getMonth();    
      if (month < 0 || (month === 0 && NOW.getDate() < BIRTHDATE.getDate())) {
        age--;
      }
      return age;
    },

    getPassword: function() {
      return firstName.charAt(0).toUpperCase() + lastName.toLowerCase() + birthday.split('.').reverse()[0];
    }
  };
    
  Object.defineProperty(newUser, 'firstName', {
    get: function() {
      return firstName;
    },
  });
    
  Object.defineProperty(newUser, 'lastName', {
    get: function() {
      return lastName;
    },
  });
    
  // Дає можливість змінювати значення firstName та lastName відповідно, що дозволяє оновлювати значення властивостей
  newUser.setFirstName = function(value) {
    firstName = value;
  };
    
  newUser.setLastName = function(value) {
    lastName = value;
  };
    
  return newUser;
}
  
const user = createNewUser();
console.log(user.getLogin());
console.log(user.getAge());
console.log(user.getPassword());